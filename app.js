var app = angular.module('myApp',[]);

app.controller('nameDisplay', function($scope,$interval){
    $scope.firstName = 'Matthew';
    $scope.lastName = 'Chuong';
    $scope.theTime = new Date().toLocaleTimeString();
    $interval(function(){
        $scope.theTime = new Date().toLocaleTimeString();
    }, 1000);
});
